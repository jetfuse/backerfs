try:
    from setuptools import setup
except ImportError:
    from distutils import setup

setup(
    name='backerfs',
    version='0.0.7',
    description='A library for mounting and formatting filesystems',
    long_description='Library for cleanly mounting and formatting filesystems by making use of a context manager',
    author='Brandon Nielsen',
    author_email='nielsenb@jetfuse.net',
    url='https://bitbucket.org/jetfuse/backerfs',
    packages=['backerfs'],
    classifiers=[
        'Intended Audience :: Developers',
        'License :: OSI Approved :: BSD License',
        'Operating System :: Unix',
        'Programming Language :: Python',
        'Programming Language :: Python :: 2',
        'Topic :: Software Development :: Libraries :: Python Modules'
    ]
)
