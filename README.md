# backerfs

## Simple filesystem mounter / formatter

### Installation

	:::bash
 	$ pip install -e git+https://bitbucket.org/jetfuse/backerfs.git#egg=backerfs

### Use

#### S3BackerFS

Used for creating, mounting, and formatting [s3backer](https://github.com/archiecobbs/s3backer/>) filesystems.

Credentials can be provided using either an IAM role (when running on EC2) or by an AWS access key id / secret key pair.

Example for mounting a filesystem (read only by default), getting the path to the root of the filesystem, and unmounting:

	:::python
	>>> from backerfs.s3backerfs import S3BackerFS
	>>> fs = S3BackerFS('s3backer_bucket_name', aws_access_key_id='ACCESS_KEY_ID', aws_secret_access_key='SECRET_KEY')
	>>> fs.mount()
	s3backer: auto-detecting block size and total file size...
	s3backer: auto-detected block size=512k and total size=16m
	s3backer: block cache size (1000) is greater that the total number of blocks (32); automatically reducing
	>>> fs.get_path('')
	'/tmp/tmpG6rEf5/'
	>>> fs.unmount()

To format a filesystem you have to provide a [mkfs](http://linux.die.net/man/8/mkfs>) command, a filesystem size (in B), and a block size (in B). For example, to create a 16M [XFS](http://oss.sgi.com/projects/xfs/>) filesystem with a 512 KiB blocksize (note that the bucket must already exist!):

	:::python
	>>> from backerfs.s3backerfs import S3BackerFS
	>>> fs = S3BackerFS('s3backer_bucket_name', aws_access_key_id='ACCESS_KEY_ID', aws_secret_access_key='SECRET_KEY')
	>>> fs.format('mkfs.xfs', 16777216, 524288)
	s3backer: auto-detecting block size and total file size...
	s3backer: auto-detection failed; using configured block size 512k and file size 16m
	s3backer: MD5 cache size (10000) is greater that the total number of blocks (32); automatically reducing
	s3backer: block cache size (1000) is greater that the total number of blocks (32); automatically reducing
	meta-data=/tmp/tmp7Kpgy4/file    isize=256    agcount=1, agsize=4096 blks
	   =                       sectsz=512   attr=2, projid32bit=1
	   =                       crc=0        finobt=0
	data     =                       bsize=4096   blocks=4096, imaxpct=25
	   =                       sunit=0      swidth=0 blks
	naming   =version 2              bsize=4096   ascii-ci=0 ftype=0
	log      =internal log           bsize=4096   blocks=853, version=2
	   =                       sectsz=512   sunit=0 blks, lazy-count=1
	realtime =none                   extsz=4096   blocks=0, rtextents=0

#### EBSBackerFS

_EBS only works on EC2 instances, additionally, boto3 installed via pip does not seem to work, producing 'botocore.exceptions.DataNotFoundError: Unable to load data for: ec2/2016-04-01/service-2' errors. Install boto3 via yum ('yum install python27-boto3')._

Credentials can be provided using either an IAM role or by an AWS access key id / secret key pair.

Mounting a filesystem (read only by default), getting the path to a file on the filesystem, and unmounting:

	:::python
	>>> from backerfs.ebsbackerfs import EBSBackerFS
	>>> fs = EBSBackerFS('ebs_volume_id')
	>>> fs.mount()
	>>> fs.get_path('coads.nc')
	'/tmp/tmp5X2GBd/coads.nc'
	>>> fs.unmount()

EBS does not support a volume being attached to multiple EC2 instances. To work around this, a snapshot of the volume can be created, a new volume created from the snapshot and then subsequently attached to the EC2 instance. Writes to this volume will not be persisted to the original volume, so it should be mounted read only (the default):

	:::python
	>>> from backerfs.ebsbackerfs import EBSBackerFS
	>>> fs = EBSBackerFS('ebs_volume_id', use_snapshot=True)
	>>> fs.mount()
	>>> fs.get_path('coads.nc')
	'/tmp/tmp_ZUT71/coads.nc'
	>>> fs.unmount()

An already existing snapshot can be used directly. A new volume will be created from the given snapshot, and deleted when unmounted:

	:::python
	>>> from backerfs.ebsbackerfs import EBSBackerFS
	>>> fs = EBSBackerFS(from_snapshot_id='ebs_snapshot_id')
	>>> fs.mount()
	>>> fs.get_path('coads.nc')
	'/tmp/tmp50Mewu/coads.nc'
	>>> fs.unmount()

To format a filesystem, a mkfs command must be provided:

	:::python
	>>> from backerfs.ebsbackerfs import EBSBackerFS
	>>> fs = EBSBackerFS('ebs_volume_id')
	>>> fs.format('mkfs.xfs')
	meta-data=/dev/sdf               isize=256    agcount=4, agsize=65536 blks
		 =                       sectsz=512   attr=2, projid32bit=1
		 =                       crc=0        finobt=0
	data     =                       bsize=4096   blocks=262144, imaxpct=25
		 =                       sunit=0      swidth=0 blks
	naming   =version 2              bsize=4096   ascii-ci=0 ftype=0
	log      =internal log           bsize=4096   blocks=2560, version=2
		 =                       sectsz=512   sunit=0 blks, lazy-count=1
	realtime =none                   extsz=4096   blocks=0, rtextents=0

### Development

Install requirements:

	:::bash
	$ pip install -e . -r dev_requirements.txt

#### Tests

To run the unit tests type in your source checkout:

	:::python
	$ python -m unittest discover backerfs/tests/
