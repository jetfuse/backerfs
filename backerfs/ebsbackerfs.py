import boto3
import datetime
import logging
import tempfile
import time
import urllib2
import os

from backerfs import BackerFS
from subprocess import call

logger = logging.getLogger(__name__)

class EBSBackerFS(BackerFS):
    def __init__(self, ebs_volume_id=None, from_snapshot_id=None, aws_iam_role=None, aws_access_key_id=None, aws_secret_access_key=None, aws_region='us-east-1', use_snapshot=False, lazy_unmount=False):
        if ebs_volume_id is None and from_snapshot_id is None:
            raise ValueError('Either a EBS volume id or a snapshot is must be provided.')

        self._ebs_volume_id = ebs_volume_id
        self._ebs_snapshot_id = from_snapshot_id

        if from_snapshot_id is not None:
            self._use_snapshot = True #Cannot instantiate from a snapshot with using a snapshot
        else:
            self._use_snapshot = use_snapshot

        self._aws_iam_role = aws_iam_role
        self._aws_access_key_id = aws_access_key_id
        self._aws_secret_access_key = aws_secret_access_key
        self._aws_region = aws_region

        #Whether unmounts should be lazy
        self._lazy_unmount = lazy_unmount

        self._backer_mount_point = tempfile.mkdtemp()

        #EBS only works inside the Amazon cloud, we need our instance id
        #
        #http://docs.aws.amazon.com/AWSEC2/latest/UserGuide/ec2-instance-metadata.html
        #http://stackoverflow.com/questions/31630822/boto3-equivalent-to-boto-utils-get-instance-metadata
        self._instance_id = urllib2.urlopen('http://169.254.169.254/latest/meta-data/instance-id').read()

        if aws_iam_role is not None:
            session = boto3.session.Session(region_name=aws_region) #The IAM credentials will be used automatically
        else:
            session = boto3.session.Session(aws_access_key_id=aws_access_key_id, aws_secret_access_key=aws_secret_access_key, region_name=aws_region)

        self._ec2 = session.resource('ec2') #Used to make EC2 calls
        self._client = self._ec2.meta.client #http://stackoverflow.com/questions/26871776/how-can-i-access-a-low-level-client-from-a-boto-3-resource-instance

        self._instance = self._ec2.Instance(self._instance_id) #Get reference to instance

        self._ebs_volume = None
        self._ebs_device_name = None

    def __reduce__(self):
        #Things from botocore aren't pickleable, so we need to handle pickling / unpickling ourselves
        pickle_tuple = (
                self._ebs_volume_id,
                self._ebs_snapshot_id,
                self._aws_iam_role,
                self._aws_access_key_id,
                self._aws_secret_access_key,
                self._aws_region,
                self._use_snapshot,
                self._lazy_unmount
            )

        state_dict = {'backer_mount_point': self._backer_mount_point, 'ebs_device_name': self._ebs_device_name}

        #Add either the instance volume id, or none
        if self._ebs_volume is not None:
            state_dict['ebs_volume'] = self._ebs_volume.id
        else:
            state_dict['ebs_volume'] = None

        return (EBSBackerFS, pickle_tuple, state_dict)

    def __setstate__(self, state):
        #Switch to the original mount point
        os.rmdir(self._backer_mount_point)
        self._backer_mount_point = state['backer_mount_point']

        if state['ebs_volume'] is not None:
            #Reconstruct the EBS volume
            self._ebs_volume = self._ec2.Volume(state['ebs_volume'])
        else:
            self._ebs_volume = None

        self._ebs_device_name = state['ebs_device_name']

    def _attach_volume(self):
        #Create a volume if we're using a snapshot, otherwise, get a reference to an existing volume
        if self._use_snapshot is True:
            if self._ebs_snapshot_id is None:
                #Create a snapshot from the given volume, this allows for multiple
                #simultaneous read-only mounts. If a snapshot is mounted for
                #writing, the changes will not persist
                #
                #https://linuxforlovers.wordpress.com/2009/04/11/sharing-amazon-elastic-block-store-among-multiple-instances/
                #https://boto3.readthedocs.io/en/latest/guide/migrationec2.html#working-with-ebs-snapshots
                snapshot = self._ec2.create_snapshot(VolumeId=self._ebs_volume_id, Description='EBSBackerFS snapshot created on {0}'.format(datetime.datetime.utcnow().isoformat()))
            else:
                #Create a volume using the given snapshot
                snapshot = self._ec2.Snapshot(self._ebs_snapshot_id)

            #Wait for the snapshot to complete
            snapshot_waiter = self._client.get_waiter('snapshot_completed')
            snapshot_waiter.wait(SnapshotIds=[snapshot.id])

            self._ebs_volume = self._ec2.create_volume(SnapshotId=snapshot.id, AvailabilityZone=self._instance.placement['AvailabilityZone'])
        else:
            self._ebs_volume = self._ec2.Volume(self._ebs_volume_id)

        #Wait for the volume to be available
        volume_waiter = self._client.get_waiter('volume_available')
        volume_waiter.wait(VolumeIds=[self._ebs_volume.id])

        #Attach the volume to the instance so we can mount it
        device_name = _get_free_device_name()

        if device_name is None:
            raise IOError('No free recommended block device names.')

        self._ebs_device_name = device_name
        self._instance.attach_volume(VolumeId=self._ebs_volume.id, Device=self._ebs_device_name)

        #Wait for up to 10 seconds for attachment
        for delay_count in xrange(0, 100):
            if os.path.exists(self._ebs_device_name) is True:
                #Volume is attached
                break

            time.sleep(0.1)

        if os.path.exists(self._ebs_device_name) is False:
            #Attach failed
            raise IOError('EBS device took too long to attach.')

        #If we created a snapshot, delete it now so we don't forget about it later
        if self._use_snapshot is True and self._ebs_snapshot_id is None:
            snapshot.delete()

    def cleanup(self):
        #Continue on, even on fail, in hopes of cleaning as much as we can
        try:
            self.unmount()
        except Exception as e:
            logger.warning('Unable to unmount EBSBackerFS: %s', e.message)

        try:
            os.rmdir(self._backer_mount_point)
        except Exception as e:
            logger.warning('Unable to remove EBSBackerFS backer mount point: %s', e.message)

    def format(self, mkfs_command):
        if self._ebs_volume is None:
            try:
                self._attach_volume()
            except Exception as e:
                raise IOError('Unable to attach EBS volume: {0}'.format(e.message))

        format_args = [mkfs_command, self._ebs_device_name]

        if call(format_args):
            raise IOError('Unable to format EBS backed filesystem.')

    def mount(self, mode='ro'):
        if self._ebs_volume is None:
            try:
                self._attach_volume()
            except Exception as e:
                raise IOError('Unable to attach EBS volume: {0}'.format(e.message))

        if mode != 'ro' and self._use_snapshot is True:
            logger.info('Mounting EBSBackerFS for writing while using a volume created from a snapshot, changes will not persist.')

        #Mount up the filesystem
        mount_args = ['mount', '-o', mode, self._ebs_device_name, self._backer_mount_point]

        if call(mount_args):
            raise IOError('Unable to mount EBS backed filesystem.')

    def unmount(self):
        unmount_args = ['umount']
        if self._lazy_unmount:
            unmount_args.append('--lazy')

        #Always unmount filesystem first, continuing even if unmount fails
        try:
            call(unmount_args + [self._backer_mount_point])
        except Exception:
            pass

        #Detach the volume
        try:
            self._ebs_volume.detach_from_instance(InstanceId=self._instance_id, Device=self._ebs_device_name)

            #Assume the volume is detached when it becomes available
            volume_waiter = self._client.get_waiter('volume_available')
            volume_waiter.wait(VolumeIds=[self._ebs_volume.id])

            #If we used a snapshot, delete the created volume
            if self._use_snapshot is True:
                self._ebs_volume.delete()

            #Clean up some state variables
            self._ebs_volume = None
            self._ebs_device_name = None
        except Exception as e:
            logger.warning('Unable to detach S3BackerFS EBS volume: %s', e.message)
            pass

    def get_path(self, filename):
        #Given a filename, returns a complete path on the mounted loopback
        return os.path.join(self._backer_mount_point, filename)

def _get_free_device_name():
    #Gets a free device name, assuming Linux, and HVM instance type, using
    #Amazon recommendations. Returns None if no recommended device name is
    #available
    #
    #http://docs.aws.amazon.com/AWSEC2/latest/UserGuide/device_naming.html

    DEVICE_PREFIX = '/dev/sd{0}'
    RECOMMENDED_DEVICE_LETTERS = ['f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p']

    for letter in RECOMMENDED_DEVICE_LETTERS:
        potential_device_name = DEVICE_PREFIX.format(letter)

        #Check if it exists, if it doesn't, assume its free
        if os.path.exists(potential_device_name) is False:
            return potential_device_name

    #Couldn't find a free letter
    return None
