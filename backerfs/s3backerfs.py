import logging
import math
import os
import tempfile
import time

from backerfs import BackerFS
from subprocess import call

logger = logging.getLogger(__name__)

class S3BackerFS(BackerFS):
    def __init__(self, bucket_name, prefix=None, aws_iam_role=None, aws_access_key_id=None, aws_secret_access_key=None, enable_compression=False, list_blocks=False, lazy_unmount=False):
        self._bucket_name = bucket_name
        self._prefix = prefix

        self._aws_iam_role = aws_iam_role
        self._aws_access_key_id = aws_access_key_id
        self._aws_secret_access_key = aws_secret_access_key

        self._backer_mount_point = tempfile.mkdtemp()
        self._loopback_mount_point = tempfile.mkdtemp()

        self._enable_compression = enable_compression
        self._list_blocks = list_blocks
        self._lazy_unmount = lazy_unmount

    def cleanup(self):
        self.unmount()

        #Continue on, even on fail, in hopes of cleaning as much as we can
        try:
            os.rmdir(self._backer_mount_point)
        except Exception as e:
            logger.warning('Unable to remove S3BackerFS backer mount point: %s', e.message)
            pass

        try:
            os.rmdir(self._loopback_mount_point)
        except Exception as e:
            logger.warning('Unable to remove S3BackerFS loopback mount point: %s', e.message)
            pass

    def format(self, mkfs_command, size, block_size):
        create_args = ['s3backer']

        if self._enable_compression:
            create_args.append('--compress')

        if self._list_blocks:
            create_args.append('--listBlocks')

        create_args.extend(['--blockSize={0}'.format(block_size), '--size={0}'.format(size)])

        if self._prefix:
            create_args.append('--prefix={0}'.format(self._prefix))

        self._append_credentials_to_args(create_args)

        create_args.append(self._bucket_name)
        create_args.append(self._backer_mount_point)

        try:
            #Create the backing filesystem
            if call(create_args):
                raise IOError('Unable to create backer filesystem.')

            #Create the loopback filesystem
            format_args = [mkfs_command, os.path.join(self._backer_mount_point, 'file')]

            if call(format_args):
                raise IOError('Unable to format backer filesystem.')
        finally:
            #Don't stay mounted
            try:
                call(['umount', self._backer_mount_point])
            except Exception as e:
                logger.warning('Unable to unmount S3BackerFS backer mount point after format: %s', e.message)
                pass

            #Hack to hang until the unmount is complete
            time.sleep(20) #TODO: This is probably way too long (call a flush?)

    def mount(self, mode='ro', size=None, block_size=None):
        mount_args = ['s3backer']

        if mode == 'ro':
            mount_args.append('--readOnly')

        if self._enable_compression:
            mount_args.append('--compress')

        if self._list_blocks:
            mount_args.append('--listBlocks')

        if size is not None:
            mount_args.append('--size={0}'.format(size))

        if block_size is not None:
            mount_args.append('--blockSize={0}'.format(block_size))

        if self._prefix:
            mount_args.append('--prefix={0}'.format(self._prefix))

        self._append_credentials_to_args(mount_args)

        mount_args.append(self._bucket_name)
        mount_args.append(self._backer_mount_point)

        #Create the backing filesystem
        if call(mount_args):
            raise IOError('Unable to mount backer filesystem.')

        #Mount the loopback
        loopback_mount_args = ['mount', '-o', ','.join([mode, 'loop']), os.path.join(self._backer_mount_point, 'file'), self._loopback_mount_point]

        if call(loopback_mount_args):
            raise IOError('Unable to mount loopbacker filesystem.')

    def unmount(self):
        unmount_args = ['umount']
        if self._lazy_unmount:
            unmount_args.append('--lazy')

        #Always unmount loopback first, continuing even if unmount fails
        try:
            call(unmount_args + [self._loopback_mount_point])
        except Exception as e:
            logger.warning('Unable to unmount S3BackerFS loopback mount point: %s', e.message)
            pass

        try:
            call(unmount_args + [self._backer_mount_point])
        except Exception as e:
            logger.warning('Unable to unmount S3BackerFS backer mount point: %s', e.message)
            pass

    def get_path(self, filename):
        #Given a filename, returns a complete path on the mounted loopback
        return os.path.join(self._loopback_mount_point, filename)

    def _append_credentials_to_args(self, args):
        if self._aws_iam_role is not None:
            args.append('--accessEC2IAM={0}'.format(self._aws_iam_role))
        else:
            args.append('--accessId={0}'.format(self._aws_access_key_id))
            args.append('--accessKey={0}'.format(self._aws_secret_access_key))

        return args

def calculate_fs_size(target_size, block_size, min_block_count=1):
    #Given a target filesystem size in bytes, and desired block size in bytes, returns
    #a filesystem size in bytes, which is:
    #1) Larger than the target filesystem size
    #2) Larger than the block_size * min_block_count
    #3) A multiple of the block size

    required_blocks = int(math.ceil(float(target_size) / float(block_size)))

    if required_blocks < min_block_count:
        return block_size * min_block_count
    else:
        return block_size * required_blocks
