import boto3
import mock
import os
import pickle
import unittest

from moto import mock_ec2
from backerfs.ebsbackerfs import EBSBackerFS, _get_free_device_name

class TestEBSBackerFSFunctions(unittest.TestCase):
    @mock_ec2
    def test_init(self):
        mock_urlopen = mock.Mock()
        mock_urlopen.read = mock.Mock(return_value='dummy_instance_id')

        with mock.patch('tempfile.mkdtemp', return_value='/dummy/tmp') as mock_mkdtemp:
            with mock.patch('urllib2.urlopen', return_value=mock_urlopen):
                testEBSBackerFS = EBSBackerFS('test_ebs_volume_id', from_snapshot_id='test_snapshot_id', aws_iam_role='test_iam_role', aws_access_key_id='test_access_key_id', aws_secret_access_key='test_secret_access_key', aws_region='us-east-1', use_snapshot=True, lazy_unmount=True)

                self.assertEqual(testEBSBackerFS._ebs_volume_id, 'test_ebs_volume_id')
                self.assertEqual(testEBSBackerFS._ebs_snapshot_id, 'test_snapshot_id')
                self.assertEqual(testEBSBackerFS._aws_iam_role, 'test_iam_role')
                self.assertEqual(testEBSBackerFS._aws_access_key_id, 'test_access_key_id')
                self.assertEqual(testEBSBackerFS._aws_secret_access_key, 'test_secret_access_key')
                self.assertEqual(testEBSBackerFS._use_snapshot, True)
                self.assertEqual(testEBSBackerFS._lazy_unmount, True)

                #In the real world, these values would hopefully be different...
                self.assertEqual(testEBSBackerFS._backer_mount_point, '/dummy/tmp')
                self.assertEqual(testEBSBackerFS._instance_id, 'dummy_instance_id')

                self.assertEqual(mock_mkdtemp.call_count, 1)

                #Make sure some values get initialized to None
                self.assertEqual(testEBSBackerFS._ebs_volume, None)
                self.assertEqual(testEBSBackerFS._ebs_device_name, None)

    def test_init_ebs_or_snapshot_id_exception(self):
        with self.assertRaises(ValueError):
            EBSBackerFS()

    @mock_ec2
    def test_pickle(self):
        #Create the mock EC2 volume
        mock_session = boto3.session.Session(region_name='us-east-1')
        mock_ec2 = mock_session.resource('ec2')

        mock_instances = mock_ec2.create_instances(ImageId='ami-6869aa05', MinCount=1, MaxCount=1) #Amazon AWI (https://aws.amazon.com/amazon-linux-ami/)
        mock_ec2_instance = mock_instances[0]

        mock_volume = mock_ec2.create_volume(Size=1, AvailabilityZone='us-east-1')

        mock_urlopen = mock.Mock()
        mock_urlopen.read = mock.Mock(return_value='dummy_instance_id')

        with mock.patch('tempfile.mkdtemp', return_value='/dummy/tmp') as mock_mkdtemp:
            with mock.patch('urllib2.urlopen', return_value=mock_urlopen):
                testEBSBackerFS = EBSBackerFS('test_ebs_volume_id', from_snapshot_id='test_snapshot_id', aws_iam_role='test_iam_role', aws_access_key_id='test_access_key_id', aws_secret_access_key='test_secret_access_key', aws_region='us-east-1', use_snapshot=True, lazy_unmount=True)

                testEBSBackerFS._ebs_volume = mock_volume
                testEBSBackerFS._ebs_device_name = '/dev/dummy'

                #Pickle
                testEBSBackerFS_pickle = pickle.dumps(testEBSBackerFS)

        #Change the tmp directory for unpickling so we can make sure the unpickled result correctly copies from the pickle
        with mock.patch('tempfile.mkdtemp', return_value='/dummy/different_tmp') as mock_mkdtemp:
            with mock.patch('urllib2.urlopen', return_value=mock_urlopen):
                with mock.patch('os.rmdir') as mock_os_rmdir:
                    #Unpickle
                    testEBSBackerFS_unpickled = pickle.loads(testEBSBackerFS_pickle)

                    self.assertEqual(testEBSBackerFS_unpickled._ebs_volume_id, 'test_ebs_volume_id')
                    self.assertEqual(testEBSBackerFS_unpickled._ebs_snapshot_id, 'test_snapshot_id')
                    self.assertEqual(testEBSBackerFS_unpickled._aws_iam_role, 'test_iam_role')
                    self.assertEqual(testEBSBackerFS_unpickled._aws_access_key_id, 'test_access_key_id')
                    self.assertEqual(testEBSBackerFS_unpickled._aws_secret_access_key, 'test_secret_access_key')
                    self.assertEqual(testEBSBackerFS_unpickled._use_snapshot, True)
                    self.assertEqual(testEBSBackerFS_unpickled._lazy_unmount, True)

                    #In the real world, these values would hopefully be different...
                    self.assertEqual(testEBSBackerFS_unpickled._backer_mount_point, '/dummy/tmp')
                    self.assertEqual(testEBSBackerFS_unpickled._instance_id, 'dummy_instance_id')
                    self.assertEqual(testEBSBackerFS_unpickled._ebs_volume, mock_volume)
                    self.assertEqual(testEBSBackerFS_unpickled._ebs_device_name, '/dev/dummy')

                    mock_os_rmdir.assert_called_once_with('/dummy/different_tmp')

    @mock_ec2
    def test_attach_volume(self):
        #Create the mock EC2 volume
        mock_session = boto3.session.Session(region_name='us-east-1')
        mock_ec2 = mock_session.resource('ec2')

        mock_instances = mock_ec2.create_instances(ImageId='ami-6869aa05', MinCount=1, MaxCount=1) #Amazon AWI (https://aws.amazon.com/amazon-linux-ami/)
        mock_ec2_instance = mock_instances[0]

        mock_volume = mock_ec2.create_volume(Size=1, AvailabilityZone='us-east-1')

        #Mock out getting the instance id
        mock_urlopen = mock.Mock()
        mock_urlopen.read = mock.Mock(return_value=mock_ec2_instance.id)

        with mock.patch('time.sleep'):
            with mock.patch('tempfile.mkdtemp', return_value='/dummy/tmp'):
                with mock.patch('urllib2.urlopen', return_value=mock_urlopen):
                    testEBSBackerFS = EBSBackerFS(mock_volume.id, aws_iam_role='test_iam_role', aws_region='us-east-1')

                    with mock.patch('backerfs.ebsbackerfs._get_free_device_name', return_value='/dev/sdf') as mock_get_free_device_name:
                        with mock.patch('os.path.exists', return_value=True) as mock_os_path:
                            testEBSBackerFS._attach_volume()

                            self.assertEqual(testEBSBackerFS._ebs_volume, mock_volume)
                            self.assertEqual(testEBSBackerFS._ebs_device_name, '/dev/sdf')

                            self.assertEqual(mock_get_free_device_name.call_count, 1)
                            self.assertEqual(mock_os_path.call_count, 2)
                            mock_os_path.assert_has_calls([mock.call('/dev/sdf'), mock.call('/dev/sdf')])

    @mock_ec2
    def test_attach_volume_snapshot(self):
        #Create the mock EC2 volume
        mock_session = boto3.session.Session(region_name='us-east-1')
        mock_ec2 = mock_session.resource('ec2')

        mock_instances = mock_ec2.create_instances(ImageId='ami-6869aa05', MinCount=1, MaxCount=1) #Amazon AWI (https://aws.amazon.com/amazon-linux-ami/)
        mock_ec2_instance = mock_instances[0]

        mock_volume = mock_ec2.create_volume(Size=1, AvailabilityZone='us-east-1')

        #Mock out getting the instance id
        mock_urlopen = mock.Mock()
        mock_urlopen.read = mock.Mock(return_value=mock_ec2_instance.id)

        with mock.patch('time.sleep'):
            with mock.patch('tempfile.mkdtemp', return_value='/dummy/tmp'):
                with mock.patch('urllib2.urlopen', return_value=mock_urlopen):
                    testEBSBackerFS = EBSBackerFS(mock_volume.id, aws_iam_role='test_iam_role', aws_region='us-east-1', use_snapshot=True)

                    with mock.patch('backerfs.ebsbackerfs._get_free_device_name', return_value='/dev/sdf') as mock_get_free_device_name:
                        with mock.patch('os.path.exists', return_value=True) as mock_os_path:
                            testEBSBackerFS._attach_volume()

                            #The snapshot gets deleted so we can't check it, but make sure the volumes are different
                            self.assertNotEqual(testEBSBackerFS._ebs_volume, mock_volume)
                            self.assertEqual(testEBSBackerFS._ebs_device_name, '/dev/sdf')

                            self.assertEqual(mock_get_free_device_name.call_count, 1)
                            self.assertEqual(mock_os_path.call_count, 2)
                            mock_os_path.assert_has_calls([mock.call('/dev/sdf'), mock.call('/dev/sdf')])

                            #Make sure there is no leftover snapshot
                            self.assertEqual(len(list(mock_volume.snapshots.all())), 0)

    @mock_ec2
    def test_attach_volume_from_snapshot(self):
        #Create the mock EC2 volume
        mock_session = boto3.session.Session(region_name='us-east-1')
        mock_ec2 = mock_session.resource('ec2')

        mock_instances = mock_ec2.create_instances(ImageId='ami-6869aa05', MinCount=1, MaxCount=1) #Amazon AWI (https://aws.amazon.com/amazon-linux-ami/)
        mock_ec2_instance = mock_instances[0]

        mock_volume = mock_ec2.create_volume(Size=1, AvailabilityZone='us-east-1')
        mock_snapshot = mock_ec2.create_snapshot(VolumeId=mock_volume.id, Description='Test snapshot for test_attach_volume_from_snapshot')

        #Mock out getting the instance id
        mock_urlopen = mock.Mock()
        mock_urlopen.read = mock.Mock(return_value=mock_ec2_instance.id)

        with mock.patch('time.sleep'):
            with mock.patch('tempfile.mkdtemp', return_value='/dummy/tmp'):
                with mock.patch('urllib2.urlopen', return_value=mock_urlopen):
                    testEBSBackerFS = EBSBackerFS(from_snapshot_id=mock_snapshot.id, aws_iam_role='test_iam_role', aws_region='us-east-1')

                    with mock.patch('backerfs.ebsbackerfs._get_free_device_name', return_value='/dev/sdf') as mock_get_free_device_name:
                        with mock.patch('os.path.exists', return_value=True) as mock_os_path:
                            testEBSBackerFS._attach_volume()

                            #Check the snapshot id
                            self.assertEqual(testEBSBackerFS._ebs_volume.snapshot_id, mock_snapshot.id)

                            #Make sure the volumes are different
                            self.assertNotEqual(testEBSBackerFS._ebs_volume, mock_volume)
                            self.assertEqual(testEBSBackerFS._ebs_device_name, '/dev/sdf')

                            self.assertEqual(mock_get_free_device_name.call_count, 1)
                            self.assertEqual(mock_os_path.call_count, 2)
                            mock_os_path.assert_has_calls([mock.call('/dev/sdf'), mock.call('/dev/sdf')])

                            #Make sure the snapshot didn't get deleted
                            self.assertEqual(list(mock_volume.snapshots.all()), [mock_snapshot])

    @mock_ec2
    def test_attach_volume_too_long(self):
        #Create the mock EC2 volume
        mock_session = boto3.session.Session(region_name='us-east-1')
        mock_ec2 = mock_session.resource('ec2')

        mock_instances = mock_ec2.create_instances(ImageId='ami-6869aa05', MinCount=1, MaxCount=1) #Amazon AWI (https://aws.amazon.com/amazon-linux-ami/)
        mock_ec2_instance = mock_instances[0]

        mock_volume = mock_ec2.create_volume(Size=1, AvailabilityZone='us-east-1')

        #Mock out getting the instance id
        mock_urlopen = mock.Mock()
        mock_urlopen.read = mock.Mock(return_value=mock_ec2_instance.id)

        with mock.patch('time.sleep'):
            with mock.patch('tempfile.mkdtemp', return_value='/dummy/tmp'):
                with mock.patch('urllib2.urlopen', return_value=mock_urlopen):
                    testEBSBackerFS = EBSBackerFS(mock_volume.id, aws_iam_role='test_iam_role', aws_region='us-east-1')

                    with mock.patch('backerfs.ebsbackerfs._get_free_device_name', return_value='/dev/sdf'):
                        with mock.patch('os.path.exists', return_value=False):
                            #Device never attaches
                            with self.assertRaises(IOError):
                                testEBSBackerFS._attach_volume()

    @mock_ec2
    def test_cleanup(self):
        mock_urlopen = mock.Mock()
        mock_urlopen.read = mock.Mock(return_value='dummy_instance_id')

        with mock.patch('tempfile.mkdtemp', return_value='/dummy/tmp'):
            with mock.patch('urllib2.urlopen', return_value=mock_urlopen):
                testEBSBackerFS = EBSBackerFS('test_ebs_volume_id')

                with mock.patch('backerfs.ebsbackerfs.EBSBackerFS.unmount') as mock_unmount:
                    with mock.patch('os.rmdir') as mock_os_rmdir:
                        testEBSBackerFS.cleanup()

                        self.assertEqual(mock_unmount.call_count, 1)
                        mock_os_rmdir.assert_called_once_with(testEBSBackerFS._backer_mount_point)

    @mock_ec2
    def test_cleanup_context_manager(self):
        mock_urlopen = mock.Mock()
        mock_urlopen.read = mock.Mock(return_value='dummy_instance_id')

        with mock.patch('tempfile.mkdtemp', return_value='/dummy/tmp'):
            with mock.patch('urllib2.urlopen', return_value=mock_urlopen):
                with mock.patch('backerfs.ebsbackerfs.EBSBackerFS.cleanup') as mock_cleanup:
                    with EBSBackerFS('test_ebs_volume_id'):
                        pass

                    self.assertEqual(mock_cleanup.called, True)

    @mock_ec2
    def test_format(self):
        #Create the mock EC2 volume
        mock_session = boto3.session.Session(region_name='us-east-1')
        mock_ec2 = mock_session.resource('ec2')

        mock_instances = mock_ec2.create_instances(ImageId='ami-6869aa05', MinCount=1, MaxCount=1) #Amazon AWI (https://aws.amazon.com/amazon-linux-ami/)
        mock_ec2_instance = mock_instances[0]

        mock_volume = mock_ec2.create_volume(Size=1, AvailabilityZone='us-east-1')

        #Mock out getting the instance id
        mock_urlopen = mock.Mock()
        mock_urlopen.read = mock.Mock(return_value=mock_ec2_instance.id)

        with mock.patch('time.sleep'):
            with mock.patch('tempfile.mkdtemp', return_value='/dummy/tmp'):
                with mock.patch('urllib2.urlopen', return_value=mock_urlopen):
                    testEBSBackerFS = EBSBackerFS(mock_volume.id, aws_iam_role='test_iam_role', aws_region='us-east-1', use_snapshot=True)
                    testEBSBackerFS._ebs_device_name = '/dev/sdf'

                    with mock.patch('backerfs.ebsbackerfs.EBSBackerFS._attach_volume'):
                        with mock.patch('backerfs.ebsbackerfs.call', return_value=0) as mock_call:
                            testEBSBackerFS.format('mkfs.dummy')

                            mock_call.assert_called_once_with(['mkfs.dummy', '/dev/sdf'])

    @mock_ec2
    def test_mount(self):
        mock_urlopen = mock.Mock()
        mock_urlopen.read = mock.Mock(return_value='dummy_instance_id')

        with mock.patch('time.sleep'):
            with mock.patch('tempfile.mkdtemp', return_value='/dummy/tmp'):
                with mock.patch('urllib2.urlopen', return_value=mock_urlopen):
                    testEBSBackerFS = EBSBackerFS('dummyVolumeId', aws_iam_role='test_iam_role', aws_region='us-east-1', use_snapshot=True)
                    testEBSBackerFS._ebs_device_name = '/dev/sdf'
                    testEBSBackerFS._backer_mount_point = '/dummy/backermount'

                    with mock.patch('backerfs.ebsbackerfs.EBSBackerFS._attach_volume'):
                        with mock.patch('backerfs.ebsbackerfs.call', return_value=0) as mock_call:
                            testEBSBackerFS.mount()

                            mock_call.assert_called_once_with(['mount', '-o', 'ro', '/dev/sdf', '/dummy/backermount'])

    @mock_ec2
    def test_mount_rw(self):
        mock_urlopen = mock.Mock()
        mock_urlopen.read = mock.Mock(return_value='dummy_instance_id')

        with mock.patch('time.sleep'):
            with mock.patch('tempfile.mkdtemp', return_value='/dummy/tmp'):
                with mock.patch('urllib2.urlopen', return_value=mock_urlopen):
                    testEBSBackerFS = EBSBackerFS('dummyVolumeId', aws_iam_role='test_iam_role', aws_region='us-east-1', use_snapshot=True)
                    testEBSBackerFS._ebs_device_name = '/dev/sdf'
                    testEBSBackerFS._backer_mount_point = '/dummy/backermount'

                    with mock.patch('backerfs.ebsbackerfs.EBSBackerFS._attach_volume'):
                        with mock.patch('backerfs.ebsbackerfs.call', return_value=0) as mock_call:
                            testEBSBackerFS.mount('rw')

                            mock_call.assert_called_once_with(['mount', '-o', 'rw', '/dev/sdf', '/dummy/backermount'])

    @mock_ec2
    def test_unmount(self):
        mock_urlopen = mock.Mock()
        mock_urlopen.read = mock.Mock(return_value='dummy_instance_id')

        #Mock the client
        mock_client = mock.Mock()
        mock_waiter = mock.Mock()
        mock_client.get_waiter = mock.MagicMock(return_value=mock_waiter)

        #Use an actual mock volume
        mock_volume = mock.Mock()
        mock_volume.id = 'dummyVolumeId'

        with mock.patch('time.sleep'):
            with mock.patch('tempfile.mkdtemp', return_value='/dummy/tmp'):
                with mock.patch('urllib2.urlopen', return_value=mock_urlopen):
                    testEBSBackerFS = EBSBackerFS('dummyVolumeId', aws_iam_role='test_iam_role', aws_region='us-east-1')
                    testEBSBackerFS._client = mock_client
                    testEBSBackerFS._ebs_volume = mock_volume
                    testEBSBackerFS._ebs_device_name = '/dev/sdf'
                    testEBSBackerFS._backer_mount_point = '/dummy/backermount'

                    with mock.patch('backerfs.ebsbackerfs.call', return_value=0) as mock_call:
                        testEBSBackerFS.unmount()

                        mock_call.assert_called_once_with(['umount', '/dummy/backermount'])

                        mock_volume.detach_from_instance.assert_called_once_with(InstanceId='dummy_instance_id', Device='/dev/sdf')
                        mock_waiter.wait.assert_called_once_with(VolumeIds=['dummyVolumeId'])
                        self.assertEqual(mock_volume.delete.called, False)

    @mock_ec2
    def test_unmount_snapshot(self):
        mock_urlopen = mock.Mock()
        mock_urlopen.read = mock.Mock(return_value='dummy_instance_id')

        #Mock the client
        mock_client = mock.Mock()
        mock_waiter = mock.Mock()
        mock_client.get_waiter = mock.MagicMock(return_value=mock_waiter)

        #Use an actual mock volume
        mock_volume = mock.Mock()
        mock_volume.id = 'dummyVolumeId'

        with mock.patch('time.sleep'):
            with mock.patch('tempfile.mkdtemp', return_value='/dummy/tmp'):
                with mock.patch('urllib2.urlopen', return_value=mock_urlopen):
                    testEBSBackerFS = EBSBackerFS('dummyVolumeId', aws_iam_role='test_iam_role', aws_region='us-east-1', use_snapshot=True)
                    testEBSBackerFS._client = mock_client
                    testEBSBackerFS._ebs_volume = mock_volume
                    testEBSBackerFS._ebs_device_name = '/dev/sdf'
                    testEBSBackerFS._backer_mount_point = '/dummy/backermount'

                    with mock.patch('backerfs.ebsbackerfs.call', return_value=0) as mock_call:
                        testEBSBackerFS.unmount()

                        mock_call.assert_called_once_with(['umount', '/dummy/backermount'])

                        mock_volume.detach_from_instance.assert_called_once_with(InstanceId='dummy_instance_id', Device='/dev/sdf')
                        mock_waiter.wait.assert_called_once_with(VolumeIds=['dummyVolumeId'])
                        self.assertEqual(mock_volume.delete.called, True)

    @mock_ec2
    def test_unmount_lazy(self):
        mock_urlopen = mock.Mock()
        mock_urlopen.read = mock.Mock(return_value='dummy_instance_id')

        #Mock the client
        mock_client = mock.Mock()
        mock_waiter = mock.Mock()
        mock_client.get_waiter = mock.MagicMock(return_value=mock_waiter)

        #Use an actual mock volume
        mock_volume = mock.Mock()
        mock_volume.id = 'dummyVolumeId'

        with mock.patch('time.sleep'):
            with mock.patch('tempfile.mkdtemp', return_value='/dummy/tmp'):
                with mock.patch('urllib2.urlopen', return_value=mock_urlopen):
                    testEBSBackerFS = EBSBackerFS('dummyVolumeId', aws_iam_role='test_iam_role', aws_region='us-east-1', lazy_unmount=True)
                    testEBSBackerFS._client = mock_client
                    testEBSBackerFS._ebs_volume = mock_volume
                    testEBSBackerFS._ebs_device_name = '/dev/sdf'
                    testEBSBackerFS._backer_mount_point = '/dummy/backermount'

                    with mock.patch('backerfs.ebsbackerfs.call', return_value=0) as mock_call:
                        testEBSBackerFS.unmount()

                        mock_call.assert_called_once_with(['umount', '--lazy', '/dummy/backermount'])

                        mock_volume.detach_from_instance.assert_called_once_with(InstanceId='dummy_instance_id', Device='/dev/sdf')
                        mock_waiter.wait.assert_called_once_with(VolumeIds=['dummyVolumeId'])
                        self.assertEqual(mock_volume.delete.called, False)

    @mock_ec2
    def test_get_path(self):
        mock_urlopen = mock.Mock()
        mock_urlopen.read = mock.Mock(return_value='dummy_instance_id')

        with mock.patch('tempfile.mkdtemp', return_value='/dummy/tmp') as mock_mkdtemp:
            with mock.patch('urllib2.urlopen', return_value=mock_urlopen):
                testEBSBackerFS = EBSBackerFS('test_ebs_volume_id')
                testEBSBackerFS._backer_mount_point = '/dev/sdf'

                self.assertEqual(testEBSBackerFS.get_path('test_filename'), os.path.join(testEBSBackerFS._backer_mount_point, 'test_filename'))

    def test_get_free_device_name(self):
         with mock.patch('os.path.exists', return_value=False) as mock_os_path:
             self.assertEqual(_get_free_device_name(), '/dev/sdf')

    def test_get_free_device_name_none_free(self):
         with mock.patch('os.path.exists', return_value=True) as mock_os_path:
             self.assertEqual(_get_free_device_name(), None)
