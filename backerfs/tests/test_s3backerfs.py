import logging
import mock
import os
import unittest

from backerfs.s3backerfs import S3BackerFS, calculate_fs_size

logging.basicConfig() #Silence "No handlers could be found for logger" errors

class TestS3BackerFSFunctions(unittest.TestCase):
    def test_init(self):
        with mock.patch('tempfile.mkdtemp', return_value='/dummy/tmp') as mock_mkdtemp:
            testS3BackerFS = S3BackerFS('test_bucket_name', prefix='test_prefix', aws_iam_role='test_iam_role', aws_access_key_id='test_access_key_id', aws_secret_access_key='test_secret_access_key', lazy_unmount=True)

            self.assertEqual(testS3BackerFS._bucket_name, 'test_bucket_name')
            self.assertEqual(testS3BackerFS._prefix, 'test_prefix')
            self.assertEqual(testS3BackerFS._aws_iam_role, 'test_iam_role')
            self.assertEqual(testS3BackerFS._aws_access_key_id, 'test_access_key_id')
            self.assertEqual(testS3BackerFS._aws_secret_access_key, 'test_secret_access_key')
            self.assertEqual(testS3BackerFS._lazy_unmount, True)

            #In the real world, these values would hopefully be different...
            self.assertEqual(testS3BackerFS._backer_mount_point, '/dummy/tmp')
            self.assertEqual(testS3BackerFS._loopback_mount_point, '/dummy/tmp')

            self.assertEqual(mock_mkdtemp.call_count, 2)

    def test_init_defaults(self):
        with mock.patch('tempfile.mkdtemp', return_value='/dummy/tmp') as mock_mkdtemp:
            testS3BackerFS = S3BackerFS('test_bucket_name')

            self.assertEqual(testS3BackerFS._bucket_name, 'test_bucket_name')
            self.assertEqual(testS3BackerFS._prefix, None)
            self.assertEqual(testS3BackerFS._aws_iam_role, None)
            self.assertEqual(testS3BackerFS._aws_access_key_id, None)
            self.assertEqual(testS3BackerFS._aws_secret_access_key, None)
            self.assertEqual(testS3BackerFS._lazy_unmount, False)

            #In the real world, these values would hopefully be different...
            self.assertEqual(testS3BackerFS._backer_mount_point, '/dummy/tmp')
            self.assertEqual(testS3BackerFS._loopback_mount_point, '/dummy/tmp')

            self.assertEqual(mock_mkdtemp.call_count, 2)

    def test_cleanup(self):
        testS3BackerFS = S3BackerFS('test_bucket_name', aws_iam_role='test_iam_role', aws_access_key_id='test_access_key_id', aws_secret_access_key='test_secret_access_key')

        #Set the temp paths
        testS3BackerFS._backer_mount_point = '/dummy/backermount'
        testS3BackerFS._loopback_mount_point = '/dummy/loopbackmount'

        with mock.patch('os.rmdir') as mock_rmdir:
            with mock.patch('backerfs.s3backerfs.call') as mock_call:
                testS3BackerFS.cleanup()

                self.assertEqual(mock_rmdir.call_count, 2)

                mock_rmdir.assert_any_call('/dummy/backermount')
                mock_rmdir.assert_any_call('/dummy/loopbackmount')

                self.assertEqual(mock_call.call_count, 2)

                mock_call.assert_any_call(['umount', '/dummy/backermount'])
                mock_call.assert_any_call(['umount', '/dummy/loopbackmount'])

    def test_cleanup_context_manager(self):
        with mock.patch('backerfs.s3backerfs.S3BackerFS.cleanup') as mock_cleanup:
            with S3BackerFS('test_bucket_name', aws_iam_role='test_iam_role', aws_access_key_id='test_access_key_id', aws_secret_access_key='test_secret_access_key'):
                pass

            self.assertEqual(mock_cleanup.called, True)

    def test_format_iam(self):
        testS3BackerFS = S3BackerFS('test_bucket_name', aws_iam_role='test_iam_role')

        #Set the temp paths
        testS3BackerFS._backer_mount_point = '/dummy/backermount'
        testS3BackerFS._loopback_mount_point = '/dummy/loopbackmount'

        with mock.patch('time.sleep'): #So tests don't take forever
            with mock.patch('backerfs.s3backerfs.call', return_value=0) as mock_call:
                testS3BackerFS.format('mkfs.dummy', 65535, 512)

                self.assertEqual(mock_call.call_count, 3)

                mock_call.assert_any_call(['s3backer', '--blockSize=512', '--size=65535', '--accessEC2IAM=test_iam_role', 'test_bucket_name', '/dummy/backermount'])
                mock_call.assert_any_call(['mkfs.dummy', os.path.join('/dummy/backermount', 'file')])
                mock_call.assert_any_call(['umount', '/dummy/backermount'])

    def test_format_access_key(self):
        testS3BackerFS = S3BackerFS('test_bucket_name', aws_access_key_id='test_access_key_id', aws_secret_access_key='test_secret_access_key')

        #Set the temp paths
        testS3BackerFS._backer_mount_point = '/dummy/backermount'
        testS3BackerFS._loopback_mount_point = '/dummy/loopbackmount'

        with mock.patch('time.sleep'): #So tests don't take forever
            with mock.patch('backerfs.s3backerfs.call', return_value=0) as mock_call:
                testS3BackerFS.format('mkfs.dummy', 65535, 512)

                self.assertEqual(mock_call.call_count, 3)

                mock_call.assert_any_call(['s3backer', '--blockSize=512', '--size=65535', '--accessId=test_access_key_id', '--accessKey=test_secret_access_key', 'test_bucket_name', '/dummy/backermount'])
                mock_call.assert_any_call(['mkfs.dummy', os.path.join('/dummy/backermount', 'file')])
                mock_call.assert_any_call(['umount', '/dummy/backermount'])

    def test_format_prefix(self):
        testS3BackerFS = S3BackerFS('test_bucket_name', prefix='test_prefix', aws_iam_role='test_iam_role')

        #Set the temp paths
        testS3BackerFS._backer_mount_point = '/dummy/backermount'
        testS3BackerFS._loopback_mount_point = '/dummy/loopbackmount'

        with mock.patch('time.sleep'): #So tests don't take forever
            with mock.patch('backerfs.s3backerfs.call', return_value=0) as mock_call:
                testS3BackerFS.format('mkfs.dummy', 65535, 512)

                self.assertEqual(mock_call.call_count, 3)

                mock_call.assert_any_call(['s3backer', '--blockSize=512', '--size=65535', '--prefix=test_prefix', '--accessEC2IAM=test_iam_role', 'test_bucket_name', '/dummy/backermount'])
                mock_call.assert_any_call(['mkfs.dummy', os.path.join('/dummy/backermount', 'file')])
                mock_call.assert_any_call(['umount', '/dummy/backermount'])

    def test_format_enable_compression(self):
        testS3BackerFS = S3BackerFS('test_bucket_name', aws_iam_role='test_iam_role', enable_compression=True)

        #Set the temp paths
        testS3BackerFS._backer_mount_point = '/dummy/backermount'
        testS3BackerFS._loopback_mount_point = '/dummy/loopbackmount'

        with mock.patch('time.sleep'): #So tests don't take forever
            with mock.patch('backerfs.s3backerfs.call', return_value=0) as mock_call:
                testS3BackerFS.format('mkfs.dummy', 65535, 512)

                self.assertEqual(mock_call.call_count, 3)

                mock_call.assert_any_call(['s3backer', '--compress', '--blockSize=512', '--size=65535', '--accessEC2IAM=test_iam_role', 'test_bucket_name', '/dummy/backermount'])
                mock_call.assert_any_call(['mkfs.dummy', os.path.join('/dummy/backermount', 'file')])
                mock_call.assert_any_call(['umount', '/dummy/backermount'])

    def test_format_list_blocks(self):
        testS3BackerFS = S3BackerFS('test_bucket_name', aws_iam_role='test_iam_role', list_blocks=True)

        #Set the temp paths
        testS3BackerFS._backer_mount_point = '/dummy/backermount'
        testS3BackerFS._loopback_mount_point = '/dummy/loopbackmount'

        with mock.patch('time.sleep'): #So tests don't take forever
            with mock.patch('backerfs.s3backerfs.call', return_value=0) as mock_call:
                testS3BackerFS.format('mkfs.dummy', 65535, 512)

                self.assertEqual(mock_call.call_count, 3)

                mock_call.assert_any_call(['s3backer', '--listBlocks', '--blockSize=512', '--size=65535', '--accessEC2IAM=test_iam_role', 'test_bucket_name', '/dummy/backermount'])
                mock_call.assert_any_call(['mkfs.dummy', os.path.join('/dummy/backermount', 'file')])
                mock_call.assert_any_call(['umount', '/dummy/backermount'])

    def test_format_finally_unmount(self):
        testS3BackerFS = S3BackerFS('test_bucket_name', aws_iam_role='test_iam_role')

        #Set the temp paths
        testS3BackerFS._backer_mount_point = '/dummy/backermount'
        testS3BackerFS._loopback_mount_point = '/dummy/loopbackmount'

        with mock.patch('time.sleep'): #So tests don't take forever
            with mock.patch('backerfs.s3backerfs.call', return_value=1) as mock_call:
                with self.assertRaises(IOError):
                    testS3BackerFS.format('mkfs.dummy', 65535, 512)

                self.assertEqual(mock_call.call_count, 2)

                mock_call.assert_any_call(['s3backer', '--blockSize=512', '--size=65535', '--accessEC2IAM=test_iam_role', 'test_bucket_name', '/dummy/backermount'])
                mock_call.assert_any_call(['umount', '/dummy/backermount'])

    def test_mount_iam(self):
        testS3BackerFS = S3BackerFS('test_bucket_name', aws_iam_role='test_iam_role')

        #Set the temp paths
        testS3BackerFS._backer_mount_point = '/dummy/backermount'
        testS3BackerFS._loopback_mount_point = '/dummy/loopbackmount'

        with mock.patch('backerfs.s3backerfs.call', return_value=0) as mock_call:
            testS3BackerFS.mount()

            self.assertEqual(mock_call.call_count, 2)

            mock_call.assert_any_call(['s3backer', '--readOnly', '--accessEC2IAM=test_iam_role', 'test_bucket_name', '/dummy/backermount'])
            mock_call.assert_any_call(['mount', '-o', 'ro,loop', os.path.join('/dummy/backermount', 'file'), '/dummy/loopbackmount'])

    def test_mount_access_key(self):
        testS3BackerFS = S3BackerFS('test_bucket_name', aws_access_key_id='test_access_key_id', aws_secret_access_key='test_secret_access_key')

        #Set the temp paths
        testS3BackerFS._backer_mount_point = '/dummy/backermount'
        testS3BackerFS._loopback_mount_point = '/dummy/loopbackmount'

        with mock.patch('backerfs.s3backerfs.call', return_value=0) as mock_call:
            testS3BackerFS.mount()

            self.assertEqual(mock_call.call_count, 2)

            mock_call.assert_any_call(['s3backer', '--readOnly', '--accessId=test_access_key_id', '--accessKey=test_secret_access_key', 'test_bucket_name', '/dummy/backermount'])
            mock_call.assert_any_call(['mount', '-o', 'ro,loop', os.path.join('/dummy/backermount', 'file'), '/dummy/loopbackmount'])

    def test_mount_rw(self):
        testS3BackerFS = S3BackerFS('test_bucket_name', aws_iam_role='test_iam_role')

        #Set the temp paths
        testS3BackerFS._backer_mount_point = '/dummy/backermount'
        testS3BackerFS._loopback_mount_point = '/dummy/loopbackmount'

        with mock.patch('backerfs.s3backerfs.call', return_value=0) as mock_call:
            testS3BackerFS.mount(mode='rw')

            self.assertEqual(mock_call.call_count, 2)

            mock_call.assert_any_call(['s3backer', '--accessEC2IAM=test_iam_role', 'test_bucket_name', '/dummy/backermount'])
            mock_call.assert_any_call(['mount', '-o', 'rw,loop', os.path.join('/dummy/backermount', 'file'), '/dummy/loopbackmount'])

    def test_mount_size(self):
        testS3BackerFS = S3BackerFS('test_bucket_name', aws_iam_role='test_iam_role')

        #Set the temp paths
        testS3BackerFS._backer_mount_point = '/dummy/backermount'
        testS3BackerFS._loopback_mount_point = '/dummy/loopbackmount'

        with mock.patch('backerfs.s3backerfs.call', return_value=0) as mock_call:
            testS3BackerFS.mount(size=65535)

            self.assertEqual(mock_call.call_count, 2)

            mock_call.assert_any_call(['s3backer', '--readOnly', '--size=65535', '--accessEC2IAM=test_iam_role', 'test_bucket_name', '/dummy/backermount'])
            mock_call.assert_any_call(['mount', '-o', 'ro,loop', os.path.join('/dummy/backermount', 'file'), '/dummy/loopbackmount'])

    def test_mount_block_size(self):
        testS3BackerFS = S3BackerFS('test_bucket_name', aws_iam_role='test_iam_role')

        #Set the temp paths
        testS3BackerFS._backer_mount_point = '/dummy/backermount'
        testS3BackerFS._loopback_mount_point = '/dummy/loopbackmount'

        with mock.patch('backerfs.s3backerfs.call', return_value=0) as mock_call:
            testS3BackerFS.mount(block_size=512)

            self.assertEqual(mock_call.call_count, 2)

            mock_call.assert_any_call(['s3backer', '--readOnly', '--blockSize=512', '--accessEC2IAM=test_iam_role', 'test_bucket_name', '/dummy/backermount'])
            mock_call.assert_any_call(['mount', '-o', 'ro,loop', os.path.join('/dummy/backermount', 'file'), '/dummy/loopbackmount'])

    def test_mount_prefix(self):
        testS3BackerFS = S3BackerFS('test_bucket_name', prefix='test_prefix', aws_iam_role='test_iam_role')

        #Set the temp paths
        testS3BackerFS._backer_mount_point = '/dummy/backermount'
        testS3BackerFS._loopback_mount_point = '/dummy/loopbackmount'

        with mock.patch('backerfs.s3backerfs.call', return_value=0) as mock_call:
            testS3BackerFS.mount()

            self.assertEqual(mock_call.call_count, 2)

            mock_call.assert_any_call(['s3backer', '--readOnly', '--prefix=test_prefix', '--accessEC2IAM=test_iam_role', 'test_bucket_name', '/dummy/backermount'])
            mock_call.assert_any_call(['mount', '-o', 'ro,loop', os.path.join('/dummy/backermount', 'file'), '/dummy/loopbackmount'])

    def test_mount_enable_compression(self):
        testS3BackerFS = S3BackerFS('test_bucket_name', aws_iam_role='test_iam_role', enable_compression=True)

        #Set the temp paths
        testS3BackerFS._backer_mount_point = '/dummy/backermount'
        testS3BackerFS._loopback_mount_point = '/dummy/loopbackmount'

        with mock.patch('backerfs.s3backerfs.call', return_value=0) as mock_call:
            testS3BackerFS.mount()

            self.assertEqual(mock_call.call_count, 2)

            mock_call.assert_any_call(['s3backer', '--readOnly', '--compress', '--accessEC2IAM=test_iam_role', 'test_bucket_name', '/dummy/backermount'])
            mock_call.assert_any_call(['mount', '-o', 'ro,loop', os.path.join('/dummy/backermount', 'file'), '/dummy/loopbackmount'])

    def test_mount_list_blocks(self):
        testS3BackerFS = S3BackerFS('test_bucket_name', aws_iam_role='test_iam_role', list_blocks=True)

        #Set the temp paths
        testS3BackerFS._backer_mount_point = '/dummy/backermount'
        testS3BackerFS._loopback_mount_point = '/dummy/loopbackmount'

        with mock.patch('backerfs.s3backerfs.call', return_value=0) as mock_call:
            testS3BackerFS.mount()

            self.assertEqual(mock_call.call_count, 2)

            mock_call.assert_any_call(['s3backer', '--readOnly', '--listBlocks', '--accessEC2IAM=test_iam_role', 'test_bucket_name', '/dummy/backermount'])
            mock_call.assert_any_call(['mount', '-o', 'ro,loop', os.path.join('/dummy/backermount', 'file'), '/dummy/loopbackmount'])

    def test_unmount(self):
        testS3BackerFS = S3BackerFS('test_bucket_name', aws_iam_role='test_iam_role', aws_access_key_id='test_access_key_id', aws_secret_access_key='test_secret_access_key')

        #Set the temp paths
        testS3BackerFS._backer_mount_point = '/dummy/backermount'
        testS3BackerFS._loopback_mount_point = '/dummy/loopbackmount'

        with mock.patch('backerfs.s3backerfs.call') as mock_call:
            testS3BackerFS.unmount()

            self.assertEqual(mock_call.call_count, 2)

            mock_call.assert_any_call(['umount', '/dummy/backermount'])
            mock_call.assert_any_call(['umount', '/dummy/loopbackmount'])

    def test_unmount_lazy(self):
        testS3BackerFS = S3BackerFS('test_bucket_name', aws_iam_role='test_iam_role', aws_access_key_id='test_access_key_id', aws_secret_access_key='test_secret_access_key', lazy_unmount=True)

        #Set the temp paths
        testS3BackerFS._backer_mount_point = '/dummy/backermount'
        testS3BackerFS._loopback_mount_point = '/dummy/loopbackmount'

        with mock.patch('backerfs.s3backerfs.call') as mock_call:
            testS3BackerFS.unmount()

            self.assertEqual(mock_call.call_count, 2)

            mock_call.assert_any_call(['umount', '--lazy', '/dummy/backermount'])
            mock_call.assert_any_call(['umount', '--lazy', '/dummy/loopbackmount'])

    def test_unmount_exception(self):
        testS3BackerFS = S3BackerFS('test_bucket_name', aws_iam_role='test_iam_role', aws_access_key_id='test_access_key_id', aws_secret_access_key='test_secret_access_key')

        #Set the temp paths
        testS3BackerFS._backer_mount_point = '/dummy/backermount'
        testS3BackerFS._loopback_mount_point = '/dummy/loopbackmount'

        with mock.patch('backerfs.s3backerfs.call', side_effect=Exception('Dummy Exception')) as mock_call:
            testS3BackerFS.unmount()

            self.assertEqual(mock_call.call_count, 2)

            mock_call.assert_any_call(['umount', '/dummy/backermount'])
            mock_call.assert_any_call(['umount', '/dummy/loopbackmount'])

    def test_get_path(self):
        testS3BackerFS = S3BackerFS('test_bucket_name', aws_iam_role='test_iam_role', aws_access_key_id='test_access_key_id', aws_secret_access_key='test_secret_access_key')

        #Set the temp paths
        testS3BackerFS._backer_mount_point = '/dummy/backermount'
        testS3BackerFS._loopback_mount_point = '/dummy/loopbackmount'

        self.assertEqual(testS3BackerFS.get_path('test_filename'), os.path.join(testS3BackerFS._loopback_mount_point, 'test_filename'))

class Tests3backerfsFunctions(unittest.TestCase):
    def test_calculate_fs_size(self):
        #With a block size of 512 bytes, and a desired filesystem size of 10 blocks, we get a target size of 5120 bytes.
        #Test default of minimum block count of 1, which should give 5120 back
        self.assertEqual(calculate_fs_size(5120, 512), 5120)

        #Set minimum block count to 100, which should force the target size to (512 * 100) = 51200
        self.assertEqual(calculate_fs_size(5120, 512, min_block_count=100), 51200)
