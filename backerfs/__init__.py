class BackerFS(object):
    def __enter__(self):
        return self

    def __exit__(self, exception_type, exception_value, traceback):
        self.cleanup()

    def cleanup(self):
        raise NotImplementedError('Class %s does not implement cleanup' % (self.__class__.__name__))

    def format(self):
        raise NotImplementedError('Class %s does not implement format' % (self.__class__.__name__))

    def mount(self):
        raise NotImplementedError('Class %s does not implement mount' % (self.__class__.__name__))

    def unmount(self):
        raise NotImplementedError('Class %s does not implement unmount' % (self.__class__.__name__))

    def get_path(self):
        raise NotImplementedError('Class %s does not implement get_path' % (self.__class__.__name__))
